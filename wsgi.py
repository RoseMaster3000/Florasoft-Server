from app import create_app
app, socketio = create_app(local=False)


if __name__ == "__main__":
    from utility import cPrint, clearScreen, getInternalIp, getExternalIp
    clearScreen()
    cPrint(f"[INT] http://{getInternalIp()}:8080")
    cPrint(f"[EXT] http://{getExternalIp()}:8080")
    socketio.run(app, host="0.0.0.0", port=8080, debug=False, log_output=False, use_reloader=False)
