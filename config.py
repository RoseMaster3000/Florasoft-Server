from pymongo import MongoClient
from os import path
from mongolog.handlers import MongoHandler
from werkzeug.middleware.proxy_fix import ProxyFix
from backend.validate import initCURRENCY, initPROFANITY

import SECRETS
LOCAL = False

# configure FLASK APP before launch
def applyConfig(APP, local=False):
    # set LOCAL flag  
    global LOCAL; LOCAL = local
    APP.config["LOCAL"] = LOCAL
    # NGINX proxy fix
    if not LOCAL: APP.wsgi_app = ProxyFix(APP.wsgi_app)

    # intialize 3rd party modules
    initCURRENCY(APP, SECRETS.CURRENCY)
    initPROFANITY(APP)

    # NAVBAR
    APP.config["NAVPAIRS"] = [
        ["🏠", "main.homePage"],
        ["💻", "netsol.netsolPage"],
        ["🤖", "robotix.gamePage"],
        ["💖", "lovelogic.gamePage"],
        ["⚔️", "tabletop.gamePage"],
        ["🤬", "developer.profanityPage"],
    ]
    APP.config["ADMINPAIRS"] = [
        ["💰", "netsol.solPage"],
        ["📄", "main.shahrosePage"],
        ["🌿", "leaflet.leafletPage"],
        ["👰", "waifus.allImagePageForward"],
        # ["💯", "waifus.allResults"],
        ["⏰", "main.countdownPage"],
        ["⚙️", "developer.developerPage"],
    ]

    # FLASK CONFIGS
    APP.config["SEND_FILE_MAX_AGE_DEFAULT"] = 0
    APP.config["ADMINS"] = ["RoseMaster3000"]
    APP.config["SECRET_KEY"] = SECRETS.FLASK
    APP.config['JSON_SORT_KEYS'] = False

    # FLASK SESSIONS CONFIGS
    APP.config["SESSION_TYPE"] = "mongodb"
    APP.config["SESSION_COOKIE_SECURE"] = True

    # MONGO CONNECTION
    mongoUser = "server"
    mongoPass = SECRETS.MONGO
    mongoDatabase = "florasoft"
    if local: mongoURL = "localhost:27017"
    else:     mongoURL = "cluster0.oa3py.mongodb.net"
    mongoAddress = f"mongodb+srv://{mongoUser}:{mongoPass}@{mongoURL}"
    # MONGO FLASK BRIDGE
    APP.config["MONGO_URI"] = f"{mongoAddress}/{mongoDatabase}?retryWrites=true&w=majority"
    APP.config["MONGO_LOGGER"] = MongoHandler.to(host=mongoAddress ,db=mongoDatabase, collection="error")    
    APP.config["SESSION_MONGODB"] = MongoClient(mongoAddress)
    APP.config["SESSION_MONGODB_DB"] = mongoDatabase
    APP.config["SESSION_MONGODB_COLLECT"] = "session"


if __name__ == "__main__":
    # SECRET.FLASK GENERATOR
    from os import urandom
    print(urandom(24))