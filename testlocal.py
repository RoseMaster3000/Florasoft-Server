if __name__ == "__main__":
    from app import create_app
    from utility import clearScreen, cPrint
    app, socketio = create_app(local=True)

    clearScreen()
    cPrint("http://127.0.0.1:5000")
    socketio.run(app, port=5000, debug=True, log_output=False, use_reloader=True)
