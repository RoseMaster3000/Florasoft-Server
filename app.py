from flask import Flask
from flask_socketio import SocketIO
from flask_jsglue import JSGlue
from flask_session_local import Session
from utility import cPrint, setLogger
from backend.database import dropCollection, pruneCollection, touchUserCollection

def create_app(local=False):
    ###############################
    # extensions instances
    ###############################
    APP = Flask(__name__.split('.')[0])
    from backend.database import MON
    from config import applyConfig; applyConfig(APP, local)
    SES = Session()
    JSG = JSGlue()
    SIO = SocketIO()
    SES.init_app(APP)
    MON.init_app(APP)
    JSG.init_app(APP)
    if local:
        SIO.init_app(APP, logger=False, engineio_logger=False, manage_session=False)
    else:
        cors = ["https://florasoft.live", "https://shahrose.live"]
        SIO.init_app(APP, logger=False, engineio_logger=False, manage_session=False, cors_allowed_origins=cors)

    ###############################
    # blueprints
    ###############################
    from pages.main import main;           APP.register_blueprint(main)
    from pages.developer import developer; APP.register_blueprint(developer)
    from pages.accounts import accounts;   APP.register_blueprint(accounts)
    from pages.waifus import waifus;       APP.register_blueprint(waifus)
    from pages.tabletop import tabletop;   APP.register_blueprint(tabletop)
    from backend.current_user import user; APP.register_blueprint(user)
    from backend.wrapper import wrapper;   APP.register_blueprint(wrapper)
    from pages.robotix import robotix;     APP.register_blueprint(robotix)
    from pages.lovelogic import lovelogic; APP.register_blueprint(lovelogic)
    from pages.leaflet import leaflet;     APP.register_blueprint(leaflet)
    from pages.netsol import netsol;       APP.register_blueprint(netsol)

    #socket stuff
    from backend.sock import baseSock;     baseSock.init_io(SIO)

    ###############################
    # things that could also be blueprinted...
    # @blueprint.before_request
    # @blueprint.after_request
    ###############################

    from flask import render_template
    @APP.errorhandler(404)
    def notFoundPage(e):
        return render_template("error.html", code=404, desc="page not found"), 404
    @APP.errorhandler(500)
    def notFoundPage(e):
        return render_template("error.html", code=500, desc="Server-Side Error"), 500

    ###############################
    # DATABASE STARTUP 
    ###############################
    cPrint("Startup Operations",end="", color="yellow")
    dropCollection("session"); cPrint(".",end="", color="yellow")
    pruneCollection("user", "sid"); cPrint(".",end="", color="yellow")
    pruneCollection("user", "sdid"); cPrint(".",end="", color="yellow")
    touchUserCollection()
    cPrint("DONE!", color="yellow")

    ###############################
    # Done! ~Return App
    ###############################
    if local: setLogger("WARNING")
    else: setLogger("ERROR")
    return APP, SIO