if __name__ == "__main__":
    from app import create_app
    from utility import clearScreen, cPrint, getInternalIp
    app, socketio = create_app(local=True)
    
    clearScreen()
    cPrint(f"http://{getInternalIp()}:4200")
    socketio.run(app, host="0.0.0.0", port=4200, debug=True, log_output=False, use_reloader=True)
    