var socket;

function disconnectSock()
{
    try {socket.disconnect();} catch {}
}

function connectSock()
{
    try {socket.disconnect();} catch {}
    socket = io("/");

    socket.on('connect', function(){
        console.log("connected to server");
        socket.send("I just connected to you!");
    });
    socket.on('disconnect', function(){
        updateOnlineTable([{"username":"(connect to see who's online)","sid":""}])
        console.log("diconnnected from server");
    });
    socket.on('message', function(message){
        console.log(message);
    });

    socket.on('loginResp', function(data){
        console.log(data);
    });

    socket.on('logoutResp', function(data){
        console.log(data);
    });

    socket.on('401', function(data){
        console.log(data);
    });

    socket.on('ALLONLINE', function(data){
        updateOnlineTable(data)
    });

}


function login(name, pass)
{
    socket.emit("login", {"username":name, "password": pass});
}

function logout()
{
    socket.emit("logout");
}

function joinAlpha(name)
{
    var getUrl = window.location;
    var nextUrl = getUrl.protocol + "//" + getUrl.host + "/developer/add/" + name
    window.location.href= nextUrl;
}

function removeAlpha(name)
{  
    var getUrl = window.location;
    var nextUrl = getUrl.protocol + "//" + getUrl.host + "/developer/remove/" + name
    window.location.href= nextUrl;
}

function refreshSite()
{  
    var getUrl = window.location;
    var nextUrl = getUrl.protocol + "//" + getUrl.host + "/refresh"
    window.location.href= nextUrl;
}

function updateOnlineTable(data)
{
    // reset table
    var old_tbody = document.getElementById('onlineTable')
    var new_tbody = document.createElement("tbody");
    new_tbody.setAttribute("id", "onlineTable");
    old_tbody.parentNode.replaceChild(new_tbody, old_tbody)

    // load in new rows
    data.forEach(appendOnlineTable)
}


function appendOnlineTable(item)
{
    // create new row
    var row = document.createElement("tr");
    var row = document.createElement("tr");
    var colA = document.createElement("td");
    var colB = document.createElement("td");
    var nodeA = document.createTextNode(item["username"]);
    var nodeB = document.createTextNode(item["sid"]);
    colA.appendChild(nodeA);
    colB.appendChild(nodeB);
    row.appendChild(colA);
    row.appendChild(colB);
    // add to table
    onlineTable = document.getElementById('onlineTable')
    onlineTable.appendChild(row);
}


// demonstrations
function pressFunc()
{
    window.alert("what's up?");
    var stuff = window.prompt("Type something lol");
    target = document.getElementById('here');
    target.append(stuff); 
}
function home()
{
    url = Flask.url_for("main.indexPage")
    console.log(url);
}
function getNum()
{
    inp = window.prompt("Input Number:", "0");
    console.log(inp)
}



// socket.emit("login", {"username":"RoseMaster3000", "password": "1s1s1s1s"});
// socket.emit("unsock");
// socket.emit("iam");
// socket.emit("exit")
