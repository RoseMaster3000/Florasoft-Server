////////////////////////////////
// EMPLOYEE DATA
////////////////////////////////
var team = [
    {"role":"Lead Developer", "rate": 45000, "count":0},
    {"role":"Mobile Developer", "rate": 24000, "count":0},
    {"role":"Business Software Analyst", "rate": 45000, "count":0},
    {"role":"Senior Software Engineer", "rate": 27500, "count":0},
    {"role":"Backend Software Developer", "rate": 32000, "count":0},
    {"role":"Digital Solution Architect", "rate": 48000, "count":0},
    {"role":"Quality Assurance Specialist", "rate": 22000, "count":0},
    {"role":"Software Support Engineer", "rate": 25000, "count":0},
    {"role":"Web Developer", "rate": 37500, "count":0},
];
var total = {
    "days":1,
    "dailyCost": 0,
    "totalCost":0,
};

////////////////////////////////
// FUNCTIONS
////////////////////////////////

// var countryBuffer = {}
// function countryIndex(countryCode)
// {
//     if (countryCode in countryBuffer){
//         return countryBuffer[countryCode];
//     }
//     else {
//         var index = indexWithAttr(app.allCurrency, "value", "PK");
//         countryBuffer.push({key:countryCode, value:index});
//         return index;
//     }    
// }

function indexWithAttr(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}

function commaNum(x) {
    x = Math.round(x)
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//////////////////////////
// BUILD VUE APP
//////////////////////////

var vuetifyOptions = { theme: {dark:true} };

var app = new Vue({
    el: '#app',
    vuetify: new Vuetify(vuetifyOptions),
    delimiters: ['[[',']]'],

    data: {
        team,
        total,
        allCurrency : [],
        curCurrency : "PK",
    },

    computed: {
        // readable string for total["days"]
        durationString() {
            var display = "";
            var temp = this.total["days"];
            var years = Math.floor(temp/365);
            temp -= years*365;
            var months = Math.floor(temp/365/12);
            temp -= months*30;
            var days = temp;

            // return "("+this.total["days"].toString()+") "+years.toString()+" "+months.toString()+" "+days.toString();

            if (years==1)
                {display+=years.toString()+" Year ";}
            if (years>1)
                {display+=years.toString()+" Years ";}

            if (months==1)
                {display+=months.toString()+" Month ";}
            if (months>1)
                {display+=months.toString()+" Months ";}

            if (days==1)
                {display+=days.toString()+" Day ";}
            if (days>1)
                {display+=days.toString()+" Days ";}  

            return display;
        },
    },

    methods: {
        // TEAM INCREMENT
        increment (index) {
            this.total["dailyCost"] += this.team[index]["rate"];
            this.team[index]["count"] += 1;
            this.refreshTotal();
        },
        decrement (index) {
            if (this.team[index]["count"]>0)
            {
                this.total["dailyCost"] -= this.team[index]["rate"];
                this.team[index]["count"] -= 1;
                this.refreshTotal();
            }
        },
        reset (index) {
            this.total["dailyCost"] -= this.team[index]["count"]*this.team[index]["rate"];
            this.team[index]["count"] = 0;
            this.refreshTotal();
        },
        // TIME INCREMENT
        incrementTime (timeKey) {
            this.total[timeKey] += 1;
            this.refreshTotal();
        },
        decrementTime (timeKey) {
            if (this.total[timeKey]>0)
            {
                this.total[timeKey] -= 1;
                this.refreshTotal();
            }
        },

        // Update Total Cost
        refreshTotal() {
            // calcualte total PKR cost 
            this.total["totalCost"] = this.total["dailyCost"] * this.total["days"] ;
            // find VUE element
            var index = indexWithAttr(this.allCurrency, "value", this.curCurrency);
            var cur = this.allCurrency[index]
            // update VUE element
            var cost = this.total["totalCost"] * cur["exchangeRate"];
            cur.amountString = commaNum(cost);
        },


    },

});

////////////////////////////////
// CURRENCY DATA (internal API)
////////////////////////////////

fetch('/currency')
    .then(response => response.json())
    .then(data => parseCURRENCY(data));

function parseCURRENCY(data)
{
    for (const [key, country] of Object.entries(data)) {
        if (key=="mytimestamp") {continue;}
        if (key=="country") {app.curCurrency=country;continue;}
        app.allCurrency.push({
            value:    key,      // key used for code (country code)
            text:     country.name, // dropdown text (country name)
            disabled: false,    // dropdown greyout state
            //selected text display
            currencyName: country.currencyName,
            currencyCode: country.currencyCode,
            exchangeRate: country.exchangeRate,
            countryEmoji: country.emoji,
            amountString: "0",
        });
    }
}
