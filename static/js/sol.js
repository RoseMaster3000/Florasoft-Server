//////////////////////////
// BUILD VUE APP
//////////////////////////

Vue.use(new VueSocketIO({
    debug: false,
    connection: '/',
}))


// QR CODE TRACKING
// https://gruhn.github.io/vue-qrcode-reader/demos/CustomTracking.html

// VUETIFY APP
// https://vuetifyjs.com/en/components/navigation-drawers/#usage
// https://vuetifyjs.com/en/components/application/#application


var vuetifyOptions = { theme: {dark:true} };
var app = new Vue({
    el: '#app',
    vuetify: new Vuetify(vuetifyOptions),
    delimiters: ['[[',']]'],

    data: {
        drawer: null,
        isConnected: false,
        socketMessage: '',
    },


    sockets: {
        connect() {
            this.isConnected = true;
        },
        disconnect() {
            this.isConnected = false;
        },
        message(data) {
            this.socketMessage = data["num"]+2;
        },
    },

    methods: {
        pingServer(data) {
            this.$socket.emit('message', "Hello there ;)");
        },
        pingVersion(data) {
            this.$socket.emit('version');
        },
        onDecode(data){
            console.log("QR stuff has happened!");
        }
    }


});
