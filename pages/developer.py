# Flask Dependancies
from flask import render_template, url_for, redirect, request
from flask import Blueprint
from utility import myRedirect
# Multi File
from backend.database import *
from backend.wrapper import login_required, anon_required, admin_required, alpha_required
from backend.validate import usernameSniff
import backend.current_user as current_user


developer = Blueprint(
    'developer',
    __name__,
    template_folder='../templates',
    static_folder='../static')

###############################
# debug
###############################
@developer.route("/developer/", methods=['GET', 'POST'])
@developer.route("/developer", methods=['GET', 'POST'])
@admin_required
def developerPage():
    # No user to display
    if request.method == "GET":
        dataDict = {}
        dataDict["username"] = "Search Users"
        dataDict["error"] = "search field blank"
        return render_template("developer.html", profile=dataDict)
    # Fetch user to display
    elif request.method == "POST":
        targetUser = request.form["username"]
        return myRedirect(url_for("developer.developerPageTarget", targetUser=targetUser))


@developer.route("/developer/add/<targetUser>")
@admin_required
def joinAlpha(targetUser):
    setAttribute("user", "username", targetUser, "alpha", True)
    return myRedirect(url_for("developer.developerPageTarget", targetUser=targetUser))

@developer.route("/developer/remove/<targetUser>")
@admin_required
def removeAlpha(targetUser):
    setAttribute("user", "username", targetUser, "alpha", False)
    return myRedirect(url_for("developer.developerPageTarget", targetUser=targetUser))


@developer.route("/developer/<targetUser>", methods=['GET', 'POST'])
@admin_required
def developerPageTarget(targetUser):
    if request.method == "POST":
        return myRedirect(url_for("developer.developerPageTarget", targetUser=request.form["username"]))
    if "@" in targetUser:
        dataDict = getDocument("user", "email", targetUser)
    else:
        dataDict = getDocument("user", "username", targetUser)
    if (dataDict==None):
        dataDict = {}
        dataDict["username"] = targetUser
        dataDict["error"] = "DNE"
    else:
        del dataDict["password"]
    return render_template("developer.html", profile=dataDict)



@developer.route("/profanity", methods=['GET', 'POST'])
def profanityPage():
    if request.method == "GET":
        return render_template(
            "profanity.html"
            )
    elif request.method == "POST":
        uSniff = usernameSniff(request.form["username"])
        if uSniff["valid"]: color="green"
        else: color="red"

        return render_template(
            "profanity.html",
            color = color,
            report = uSniff["report"]
            )

# @developer.route("/get/<attr>")
# @admin_required
# def getPage(attr):
#     data = current_user.get(attr, "not set")
#     return render_template("dump.html", title="Session Get", content=[data])


# @developer.route("/set/<attr>")
# @admin_required
# def setPage(attr):
#     current_user.set(attr, "Bingo, Bongo!")
#     return render_template("dump.html", title="Session Get", content=[f"{value} is set."])
