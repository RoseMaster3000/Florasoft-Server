from flask import render_template, url_for, redirect, request
from flask import Blueprint
import os
from backend.wrapper import admin_required, login_required, alpha_required
import backend.current_user as current_user
from utility import myRedirect

robotix = Blueprint(
    'robotix',
    __name__,
    template_folder='../templates',
    static_folder='../static')


###############################
# Fetch Game Builds
###############################

file_list = []
def refreshBuilds():
    global file_list
    global url_list
    folder = os.path.join(robotix.static_folder, "robotix", "builds")
    if not (os.path.isdir(folder)): os.mkdir(folder)
    file_list = sorted(os.listdir(folder), reverse=True)
    file_list = [file for file in file_list if file.endswith(".exe")]

@robotix.before_app_first_request
def startupRefresh():
    refreshBuilds()

@robotix.route("/refresh")
@admin_required
def manualRefresh():
    refreshBuilds()
    return "Builds Refreshed"


###############################
# Game Download Pages
###############################

@robotix.route("/robotix")
def gamePage():
    return render_template("robotixDownload.html", file_list=file_list, alpha_user=current_user.get("alpha"))


@robotix.route("/robotix-build")
def buildNumber():
    return len(file_list)


@robotix.route("/robotix-download")
@alpha_required
def gameDownload():
    if (len(file_list)==0): return myRedirect(url_for("robotix.gamePage"))
    return myRedirect(url_for('static', filename=f'robotix/builds/{file_list[0]}'))
