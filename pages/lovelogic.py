from flask import render_template, url_for, redirect, request
from flask import Blueprint
import os
from backend.wrapper import admin_required, login_required, alpha_required
from utility import myRedirect

lovelogic = Blueprint(
    'lovelogic',
    __name__,
    template_folder='../templates',
    static_folder='../static')



###############################
# Game Download Pages
###############################

@lovelogic.route("/love-logic")
def gamePage():
    # render webpage
    return render_template("lovelogicDownload.html")


@lovelogic.route("/love-logic-download")
@alpha_required
def gameDownload():
    myRedirect("https://rosemaster3000.itch.io/spring-in-love")

