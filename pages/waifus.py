from flask import render_template, url_for, redirect, request
from flask import Blueprint
import os
from backend.wrapper import login_required, anon_required, admin_required
from utility import myRedirect

waifus = Blueprint(
    "waifus", __name__, template_folder="../templates", static_folder="../static"
)


##################################
# waifu functions
##################################


def getWaifuLink(franchise, name):
    folder = os.path.join(waifus.static_folder, "waifu", franchise)
    file_list = os.listdir(folder)
    # file_list = [ name for name in os.listdir(folder) if name.endswith() ]
    for f in file_list:
        if f.startswith(name):
            return url_for("static", filename="waifu/{}/{}".format(franchise, f))


def getWaifuPath(franchise, name):
    folder = os.path.join(waifus.static_folder, "waifu", franchise)
    for f in os.listdir(folder):
        if f.startswith(name + "."):
            ext = f.split(".")[1]
            wpath = os.path.join(folder, name)
            return wpath, ext
    raise Exception("Could not find {} in {}".format(name, franchise))


##################################
# waifu pages
##################################

# api list ALL franchises
@waifus.route("/waifu")
def franchisePage():
    folder = os.path.join(waifus.static_folder, "waifu")
    file_list = os.listdir(folder)
    return "<br/>".join(file_list)


# api list a franchise's images
@waifus.route("/waifu/<franchise>")
def waifuPage(franchise):
    folder = os.path.join(waifus.static_folder, "waifu", franchise)
    file_list = os.listdir(folder)
    return "<br/>".join(file_list)


# all results
@waifus.route("/results")
def allResults():
    result_path = os.path.join(waifus.static_folder, "waifu", "RESULTS.txt")
    with open(result_path, "r") as f:
        return render_template(
            "dump.html",
            title="Waifu Wars Results",
            content=f.read().replace("\n\n", "\n").split("\n"),
        )


# overwrite image (upload link)
@waifus.route("/waifu/<franchise>/<name>/overwrite", methods=["GET", "POST"])
@admin_required
def imageOverwrite(franchise, name):
    f = request.files["file"]
    if request.method == "POST":
        # delete old file
        wpath, ext = getWaifuPath(franchise, name)
        os.remove(wpath + "." + ext)
        # save new file
        ext = f.filename.split(".")[1]
        f.save(wpath + "." + ext)
    return myRedirect(url_for("waifus.allImagePage", franchise=franchise))


# friendly GUI to view images
@waifus.route("/waifus")
def allImagePageForward():
    return myRedirect(url_for("waifus.allImagePage", franchise="fgo"))


@waifus.route("/waifus/<franchise>")
def allImagePage(franchise):
    # get franchise list
    folder = os.path.join(waifus.static_folder, "waifu")
    franchise_list = [
        name for name in os.listdir(folder) if os.path.isdir(os.path.join(folder, name))
    ]
    franchise_list.sort()
    franchise_list.remove(franchise)
    franchise_list.insert(0, franchise)

    # get name_list / url_list
    folder = os.path.join(waifus.static_folder, "waifu", franchise)
    name_list = os.listdir(folder)
    name_list.sort()
    url_list = []
    for i, f in enumerate(name_list):
        name_list[i] = name_list[i].split(".")[0].split("=v=")[0].title()
        url_list.append(getWaifuLink(franchise, f))

    return render_template(
        "waifuFranchise.html",
        franchise=franchise,
        franchise_list=franchise_list,
        url_list=url_list,
        name_list=name_list,
        count=len(url_list),
    )


# jump to static image on file
@waifus.route("/waifu/<franchise>/<name>")
def imagePage(franchise, name):
    name = name.split(".")[0]
    return myRedirect(getWaifuLink(franchise, name))
