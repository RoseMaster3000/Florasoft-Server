from flask import render_template, url_for, redirect, request
from flask import Blueprint

tabletop = Blueprint(
    'tabletop',
    __name__,
    template_folder='../templates',
    static_folder='../static')



# rp design doc
@tabletop.route("/fantasy-cards")
def gamePage():
    return render_template("fantasycardsDownload.html")

# rp design doc
@tabletop.route("/fantasy-cards/design-document")
def designPage():
    return render_template("rp.html")
