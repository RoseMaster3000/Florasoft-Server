from flask import Blueprint, render_template
from flask import url_for, redirect, request, make_response
from utility import myRedirect

main = Blueprint(
    'main',
    __name__,
    template_folder='../templates',
    static_folder='../static')

@main.route("/")
def indexPage():
    return myRedirect(url_for("main.homePage"))

@main.route("/home")
def homePage():
    return render_template("home.html")

@main.route("/privacy")
def privacyPage():
    return render_template("privacy.html")

@main.route("/service")
def servicePage():
    return render_template("service.html")

@main.route("/LLC")
def llcPage():
    return render_template("pdf.html", pdfPath="shahrose/LLC.pdf")

@main.route("/countdown")
def countdownPage():
    return render_template("countdown.html")


@main.route("/shahrose")
def shahrosePage():
    return render_template("pdf.html", pdfPath="shahrose/resume.pdf")
@main.route("/resume")
def resumePage():
	return render_template("pdf.html", pdfPath="shahrose/resume.pdf")

@main.route("/discord")
def discordPage():
    body =  """
    \"FLORASOFT.LIVE/DISCORD\" is a web app that lets you create Discord.com profile links!
    For example, to go see RoseMaster#3000's discord profile simply visit 
    <a href=https://florasoft.live/discord/RoseMaster3000>florasoft.live/discord/RoseMaster3000</a>!
    """
    return render_template("shahrose.html", content=body)


@main.route("/discord/<username>")
def discordProfile(username):
    return "This does not work yet..."
    #discord_id = discord.api.getId(username)
    #myRedirect("https://discordapp.com/users/"+discord_id)