# Flask Dependancies
from flask import render_template, url_for, redirect, request, session
from flask import Blueprint
# Multi File
from utility import myRedirect
from backend.wrapper import login_required, anon_required, admin_required
import backend.current_user as current_user
from backend.current_user import ALLONLINE


accounts = Blueprint(
    "accounts",
    __name__,
    template_folder='../templates',
    static_folder='../static')

###############################
# acccount views
###############################

@accounts.route("/me")
@login_required
def profilePage():
    return render_template(
        "dictionary.html",
        title=session["username"],
        dictionary=session)


@accounts.route("/logout")
@login_required
def logoutPage():
    current_user.logout()
    return myRedirect(url_for("accounts.loginPage"))


###############################
# login + signup (web hooks)
###############################

@accounts.route("/login", methods=['GET', 'POST'])
@anon_required
def loginPage():
    if request.method == "GET":
        return render_template("login.html", targetUser=None)
    elif request.method == "POST":
        form = request.form
        if "password_verify" in form:
            return render_template("login.html", targetUser=form["username"])
        success, report = current_user.login(form)
        if success:
            return myRedirect(url_for("accounts.profilePage"))
        else:
            return render_template("login.html", targetUser=form["username"], report=report)


@accounts.route("/register", methods=['GET', 'POST'])
@anon_required
def registerPage():
    if request.method == "GET":
        return render_template("register.html")
    elif request.method == "POST":
        form = request.form
        success, report = current_user.signup(form)
        if success:
            return myRedirect(url_for("accounts.loginPage"), code=307)
        else:
            return render_template("register.html", report=report)
