from flask import render_template, Blueprint
from flask import url_for, redirect, request, make_response

from utility import *
from backend.validate import personalCurrency, getCURRENCY, ipSniff

netsol = Blueprint(
    'netsol',
    __name__,
    template_folder='../templates',
    static_folder='../static')


@netsol.route("/NETSOL")
def netsolPage():
    return render_template("netsol.html")

@netsol.route("/SOL")
def solPage():
    return render_template("sol.html")



# get CURRECNY data dict
@netsol.route("/currency")#, methods=['GET'])
def currencyAPI():
    data = getCURRENCY()
    data["country"] = ipSniff(request).get("country","PK")
    return data

# get PKR equivalant of client's country
@netsol.route("/currency/<amount>")#, methods=['GET'])
def currencyAutoAPI(amount):
    return personalCurrency(request, amount)

# get PKR equivalant of specified [country]
@netsol.route("/currency/<amount>/<country>")#, methods=['GET'])
def currencyManualAPI(amount, country):
    if country=="local": country=None
    currencyData = personalCurrency(request, amount, country)
    return currencyData


@netsol.route("/ipsniff")
def ipSniffAPI():
    # ip data
    ipData =  ipSniff(request)
    ipData.pop("readme",None)
    # money data
    countryCode = ipData["country"]
    moneyData = getCURRENCY()[countryCode]
    # return both
    ipData.update(moneyData)
    return ipData



'''
(1) The person day rate is for one single individual person that will be 
providing professional services. E.g. 2 Applications developers will 
be charged as follows (2 x 24,000 x No. of business days worked) 
and so on.

(2) Person day consists of 8 working hours. 

(3) NETSOL will charge the resources on Time and Material basis. The 
resources that are allocated on a project will be charged as per the 
rate card and the number of days resources are sourced out to 
Customer.

(4) The person day rates mentioned are the off-site rates. In case, 
resources are required on-site, these rates will be revised 
accordingly.

(5) In case, resources are required on-site, per diem and travelling 
costs will be charged separately and on actual.

(6) The rates are subjected to revision every year.

(7) These rates are heavily discounted for Customers.

(8) 16% sales tax is included in the person day quote. Any additional 
applicable taxes will be discussed and charged separately to the 
customer in the invoice.

(9) The proposal is valid till June 15th, 2021

'''
