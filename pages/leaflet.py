from flask import render_template, Blueprint
# from flask import url_for, redirect, request, make_response
# from utility import *

leaflet = Blueprint(
    'leaflet',
    __name__,
    template_folder='../templates',
    static_folder='../static')


@leaflet.route("/leaflet")
def leafletPage():
    return render_template("leaflet.html")