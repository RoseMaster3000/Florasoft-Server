import os, json, sys, logging
# from  urllib.request import urlopen
from requests import get as urlopen

from flask import redirect, request
from socket import socket, AF_INET, SOCK_DGRAM

###########################
# Logging
###########################

levelDict = {
    "CRITICAL": 50,
    "ERROR": 40,
    "WARNING": 30,
    "INFO": 20,
    "DEBUG":10,
    "NOTSET":0
}
def setLogger(level):
    # convert level
    level = level.strip().upper()
    level = levelDict[level]
    # set level
    logging.getLogger().setLevel(level)
    for logger in [logging.getLogger(name) for name in logging.root.manager.loggerDict]:
        logger.setLevel(level)
        i = 1
        for handler in logger.handlers:
            handler.setLevel(logging.ERROR)
            i += 1

###########################
# Time Utilities
###########################

from random import randint
from time import time as epoch
EPOCH_MS = lambda: int(round(epoch() * 1000))
EPOCH = lambda: int(epoch())

def chronological(sooner, later):
    if sooner == later:
        return (randint(0,1) > 0)
    else:
        return (later > sooner)

def floatify(integer):
    if integer >= 0:
        return float(f"0.{str(integer)[::-1]}")
    else:
        return float(f"-0.{str(-integer)[::-1]}")

###########################
# IP / Routing
###########################

def requestText(website):
    return urlopen(website).text

def requestJson(website):
    data = requestText(website)
    if data==None: return None
    return parseJson(data)

def getExternalIp():
    return requestText('https://api.ipify.org/?format=txt')
    #IP6 - https://ident.me
    

def getInternalIp():
    s = socket(AF_INET, SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    internalIp = s.getsockname()[0]
    s.close()
    return internalIp

# generate context aware route
def myRedirect(route, code=302):
    from config import LOCAL
    if LOCAL: return redirect(route, code=code)
    if request.is_secure:
        return redirect(f"https://florasoft.live{route}", code=code)
    else:
        return redirect(f"http://florasoft.live{route}", code=code)


###########################
# Terminal I/O
###########################

def announce(message, width=40):
    print(BOLD, end="")
    printLine(width)
    print(message.upper().center(width))
    printLine(width)
    print(ENDC, end="")

def printLine(width=40):
    print("─"*width)

def clearScreen():
    if os.name == 'nt': os.system('cls')
    else: os.system('clear')

TEXT  = {
    "black": "\u001b[30m",
    "red": "\u001b[31m",
    "green": "\u001b[32m",
    "yellow": "\u001b[33m",
    "darkblue": "\u001b[34m",
    "pink": "\u001b[35m",
    "blue": "\u001b[36m",
    "white": "\u001b[37m",
    "default": "\u001b[37m" # WHITE
}
BACKGROUND = {
    "black": "\u001b[40m",
    "red": "\u001b[41m",
    "green": "\u001b[42m",
    "yellow": "\u001b[43m",
    "darkblue": "\u001b[44m",
    "magenta": "\u001b[45m",
    "blue": "\u001b[46m",
    "white": "\u001b[47m",
    "default": "" # BLACK
}

def setTextColor(code):
    code = code.lower().replace(" ", "")
    global CUR_TEXT; CUR_TEXT = code
    sys.stdout.write(TEXT[code])
    sys.stdout.write(BACKGROUND[CUR_BACKGROUND])

def setBackColor(code):
    code = code.lower().replace(" ", "")
    global CUR_BACKGROUND; CUR_BACKGROUND = code
    sys.stdout.write(BACKGROUND[code])
    sys.stdout.write(TEXT[CUR_TEXT])

def cPrint(text, color=None, end="\n"):
    # print supression
    if NO_CPRINT:
        return
    # dict print
    elif type(text)==dict:
        for key in text:
            text = "{}{:<20}{}{}".format(TEXT[color], key, text[key], TEXT["default"])
    # list print
    elif type(text)==list:
        for item in text:
            cPrint(item, color, end);
    # print
    else:
        if color!=None:
            text = "{}{}{}".format(TEXT[color], text, TEXT["default"])
        print(text, end=end)


###########################
# FILE I/O  (+JSON)
###########################

# write files
def writeText(content, filePath):
    with open(filePath, "w") as file:
        file.write(content)

def writeJson(data, filePath, indent=4):
    if type(data)==str: data = json.loads(data)
    jsonString = makeJson(data, indent)
    with open(filePath,"w") as file:
        file.write(jsonString)

# read files
def readText(filePath):
    with open(filePath, "w") as file:
        data = file.read()
    return data

def readJson(filePath):
    with open(filePath, "r") as file:
        data = json.load(file)
    return data

def parseJson(jsonString):
    return json.loads(jsonString)

def makeJson(data, indent=4):
    return json.dumps(data, indent=indent, sort_keys=False)

def isFile(filePath):
    return os.path.isfile(filePath)

# list files/folders
def listDirectory(dirPath=None):
    return os.listdir(dirPath)

def listFolders(dirPath=None):
    for file in listDirectory(dirPath):
        path = joinPath(dirPath, file)
        if not isFile(path):
            yield path

def listFiles(dirPath=None):
    for file in listDirectory(dirPath):
        path = joinPath(dirPath, file)
        if isFile(path):
            yield path

def joinPath(*args):
    if len(args)==0:
        return ""
    elif args[0]==None:
        return os.path.join(*args[1:])
    else:
        return os.path.join(*args)
    

###########################
# UTILITY INITIALIZATION
###########################

if os.environ.get("NO_CPRINT",False): NO_CPRINT=True
elif logging.getLogger().getEffectiveLevel()>=40: NO_CPRINT=True
else: NO_CPRINT=False

CUR_TEXT = "default"
CUR_BACKGROUND = "default"
setTextColor(CUR_TEXT)
setBackColor(CUR_BACKGROUND)