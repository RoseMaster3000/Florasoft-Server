from pymongo import MongoClient, ASCENDING
from pymongo import errors as pymongo_errors
from pymongo.collation import Collation

from bson.objectid import ObjectId
from datetime import datetime

from flask import request, render_template
from flask_pymongo import PyMongo
MON = PyMongo()

###############################
# Collections
###############################

def dropCollection(collection):
    coll = MON.db[collection]
    coll.drop()

# Gets DOCUMENTS where field==value ~> list of dicts [keys: attributes]
def filterCollection(collection, field, value, *attributes):
    if len(attributes)==0: return []
    coll = MON.db[collection]
    # generate projection dict
    projectionDict = {}
    projectionDict["_id"] = 0
    for attr in attributes:
        projectionDict[attr] = 1
    # get documents
    return coll.find({field:value}, projectionDict)

# Gets DOCUMENTS where attributes exist ~> list of dicts [keys: attributes]
def distillCollection(collection, *attributes):
    coll = MON.db[collection]
    # generate dicts
    filterDict = {}
    projectionDict = {"_id":0}
    for attr in attributes:
        if attr == "_id": attr = ObjectId(attr)
        filterDict[attr] = {"$exists":True}
        projectionDict[attr] = 1
    # get documents
    return coll.find(filterDict, projectionDict)

# Removes attributes from all DOCUMENTS in a collection
def pruneCollection(collection, *attributes):
    coll = MON.db[collection]
    updateDict = {}
    # sanatize
    for attr in attributes:
        updateDict[attr]=""
    # update document
    updateDict = {"$unset":updateDict}
    coll.update_many({}, updateDict)

# create user collection if it does not exist
def touchUserCollection():
    if "user" not in MON.db.list_collection_names():
        print("Initializing \"user\" collection...")
        userCollection = MON.db.create_collection('user', collation=Collation(locale='en_US', strength=1))
        userCollection.create_index([("username", ASCENDING)], unique=True)
        userCollection.create_index([("email", ASCENDING)], unique=True)
        # EQUIVALENT Mongo:
        # db.createCollection("user", { collation: {locale: 'en_US', strength: 1} } )
        # db.user.createIndex( {email: 1}, {unique: true} )
        # db.user.createIndex( {username: 2}, {unique: true} )

###############################
# Documents
###############################

def dropDocument(collection, field, value):
    coll = MON.db[collection]
    coll.delete_one({field:value})

def addDocument(collection, documentDict):
    coll = MON.db[collection]
    # sanatize
    for key in documentDict:
        if documentDict[key]=="Date()":documentDict[key]=datetime.utcnow()
    # add document
    try:
        insertId = coll.insert_one(documentDict).inserted_id
        return True, insertId
    except pymongo_errors.DuplicateKeyError as e:
        dupe = list(e._OperationFailure__details["keyPattern"].keys())[0]
        return False, dupe
    except Exception as e:
        return False, str(e)
        # dupe = str(e).split("{")[1].split(":")[0].strip()
        # print(e._message)
        # print(e._error_labels)
        # print(e._OperationFailure__code)
        # print(e._OperationFailure__details)

def getDocument(collection, field, value=None):
    coll = MON.db[collection]
    # get document
    if (value==None and type(field)==dict):
        return coll.find_one(field)
    else:
        if field == "_id": value = ObjectId(value)
        return coll.find_one({field:value})

def updateDocument(collection, field, value, updateDict):
    coll = MON.db[collection]
    # sanatize
    if field == "_id": value = ObjectId(value)
    for key in updateDict:
        if updateDict[key]=="Date()":updateDict[key]=datetime.utcnow()
    # update document
    updateDict = {"$set":updateDict}
    filterDict  = {field : value}
    coll.update_one(filterDict, updateDict, upsert=False)


###############################
# Attributes (Fields)
###############################

def getAttribute(collection, field, value, attribute):
    coll = MON.db[collection]
    filterDict  = {field : value}
    return coll.find_one(filterDict, {attribute: 1, _id: 0 })

def setAttribute(collection, field, value, attribute, attributeValue):
    coll = MON.db[collection]
    updateDict = {}
    # sanatize
    if field == "_id": value = ObjectId(value)
    for key in updateDict:
        if updateDict[key]=="Date()":updateDict[key]=datetime.utcnow()
    # update document
    updateDict = {"$set":{attribute:attributeValue}}
    filterDict  = {field : value}
    coll.update_one(filterDict, updateDict, upsert=False)


def rmAttribute(collection, field, value, *attribute):
    rmAttributes(collection, field, value, *attribute)
def rmAttributes(collection, field, value, *attributes):
    coll = MON.db[collection]
    updateDict = {}
    # sanatize
    if field == "_id": value = ObjectId(value)
    for attr in attributes:
        updateDict[attr]=""
    # update document
    updateDict = {"$unset":updateDict}
    filterDict  = {field : value}
    coll.update_one(filterDict, updateDict)
