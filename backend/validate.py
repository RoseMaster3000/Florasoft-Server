from validate_email import validate_email
import wordninja
from copy import deepcopy
import unidecode

from utility import *
CURRENCY = None  # dictionary of ISO2CountryCodes
PROFANITY = None # [better_profanity] module object
GAPP,GKEY = None,None  # cached values for initCurrency(None,None)

#####################
# INITIALIZERS
#####################

def initPROFANITY(APP):
    from better_profanity import profanity
    global PROFANITY; PROFANITY = profanity
    root = APP.root_path if (APP!=None) else '..'
    profanityPath = joinPath(root, 'static', 'lookup', 'profanity.txt')
    PROFANITY.load_censor_words_from_file(profanityPath)#,whitelist_words=['nignog'])

def initCURRENCY(APP, key):
    if APP!=None and key!=None:
        global GAPP, GKEY
        GAPP,GKEY = APP,key
    else:
        APP,key = GAPP,GKEY
    # ISO2 country code >> [flag.svg] link
    flagData = lookupJson(APP,
        jsonLink = "https://raw.githubusercontent.com/risan/country-flag-emoji-json/main/dist/by-code.json",
        jsonFileName = "flags.json",
        expiry = 60)
    # ISO2 country code >> Currency Code
    currencyCodeData = lookupJson(APP,
        jsonLink = "http://country.io/currency.json",
        jsonFileName = "currencyCode.json",
        expiry = 365)
    # Currency Code >> Currency Name
    currencyNameData = lookupJson(APP,
        jsonLink = "http://openexchangerates.org/api/currencies.json",
        jsonFileName = "currencyName.json",
        expiry = 365)
    # Currency Code >> (float) exchange rate against PKR
    exchangeData = lookupJson(APP,
        jsonLink = f"http://api.exchangeratesapi.io/v1/latest?access_key={key}",
        jsonFileName = "currencyExchange.json",
        expiry = 1)
    exchangeData = exchangeData["rates"]

    # adjust for new BASE currency (PKR) 
    eur_in_pkr = 1 / exchangeData["PKR"]
    for key in exchangeData:
        exchangeData[key] = exchangeData[key] * eur_in_pkr

    # delete unwanted data for CURRENCY
    del flagData["mytimestamp"] # timestamp
    del flagData["UN"]          # United Nations 
    del flagData["EU"]          # European Union

    # merge all JSON data
    for countryCode in flagData:
        currencyCode = currencyCodeData.get(countryCode, "???")
        if currencyCode=="":currencyCode="???"
        currencyName = currencyNameData.get(currencyCode, "???")
        exchangeRate = exchangeData.get(currencyCode, "???")
        # if no exchange rate, switch to USD
        if exchangeRate=="???":
            currencyCode = "USD"
            currencyName = currencyNameData.get(currencyCode, "???")
            exchangeRate = exchangeData.get(currencyCode, "???")
        # merge data into [flagData]
        flagData[countryCode]["countryCode"] = countryCode
        flagData[countryCode]["currencyCode"] = currencyCode
        flagData[countryCode]["currencyName"] = currencyName
        flagData[countryCode]["exchangeRate"] = exchangeRate

    global CURRENCY
    CURRENCY = sortDictDict(flagData, "name")
    CURRENCY["mytimestamp"] = EPOCH()
    debugPath = joinPath(APP.root_path, 'static', 'lookup', 'CURRENCY.json')
    writeJson(CURRENCY, debugPath)

# download JSON data from API
def lookupJson(APP, jsonLink, jsonFileName, expiry=1):
    jsonPath = joinPath(APP.root_path, 'static', 'lookup', jsonFileName)
    data = None
    if isFile(jsonPath):
        data = readJson(jsonPath)
    if (data==None) or (EPOCH()-data["mytimestamp"]>86400*expiry):
        data = requestJson(jsonLink)
        data["mytimestamp"] = EPOCH()
        writeJson(data, jsonPath)
    return data

# sorts "dict of dict" relative to a specified "internal key"
def sortDictDict(inputDict, sortKey):
    sortedDict = {}
    for s in sorted(inputDict.items(), key=lambda k_v: k_v[1][sortKey]):
        sortedDict[s[0]] = s[1]
    return sortedDict



#######################
# DICT Report Tools
#######################


# get CURRENCY (dict of country data / exchange rates)
def getCURRENCY(countryCode=None):
    # make sure CURRENCY is up to date
    timestamp = CURRENCY.get("mytimestamp")
    if (EPOCH()-timestamp>86400):
        initCURRENCY(None,None)
    # fetch country currency data
    if countryCode==None:
        return CURRENCY
    else:
        return CURRENCY.get(countryCode, "PK")

# DICT report > PKR converted to their request's county's currency
def personalCurrency(request, pkrAmount, countryCode=None):
    pkrAmount = float(pkrAmount)
    if countryCode==None:
        countryCode = ipSniff(request).get("country","PK")
    currencyData = getCURRENCY(countryCode)
    currencyData["pkr"] = pkrAmount
    currencyData["amount"] = pkrAmount * currencyData["exchangeRate"]
    return currencyData

# DICT report > metadata on requesters IP Address
def ipSniff(request):
    addr = request.remote_addr
    if addr in ["127.0.0.1", ""]:
        url = 'https://ipinfo.io/json'
    else:
        url = 'https://ipinfo.io/' + addr + '/json'
    data = requestJson(url)
    # if data==None (API limit / connection failure?)

    if "loc" in data:
        data["latitude"]  = float(data["loc"].split(",")[0])
        data["longitude"] = float(data["loc"].split(",")[1])
    else:
        data["latitude"]  = 0
        data["longitude"] = 0
    data["address"] = addr
    return data


# DICT repost > is Email Address real?
def emailSniff(email):
    ## [py3-validate-email==0.2.8]
    valid = validate_email(
        email_address=email,
        check_regex=True,  # email format
        check_mx=False,    # host has SMPT Server + SMPT Server has this user
        from_address='RoseMaster3000@gmail.com',
        helo_host='smtp.gmail.com',
        smtp_timeout=10,
        dns_timeout=10,
        use_blacklist=True)
    return {"valid":valid, "email":email}


# DICT report > is username is valid/clean?
def usernameSniff(username):
    username = username.strip()
    if len(username) >= 25:
        return {"valid":False, "report": f"\"{username}\" is too long (25 char maximum)"}
    if len(username) <= 3:
        return {"valid":False, "report": f"\"{username}\" is too short (4 char minimum)"}
    if not username.isalnum():
        return {"valid":False, "report": f"{username} is not alpha-numeric (symbols are not permitted)"}
    bad, vision = badWordDetector(username)
    if bad:
        return {"valid":False, "report": f"Profanity in {username} ({vision})"}
    else:
        return {"valid":True,  "report": f"\"{username}\" looks good!"}


###############################
# USERNAME SNIFF COMPONENTS
###############################

# returns True if bad words
def badWordDetector(string, debug=False):
    # leet speak converter
    possibilities = leetPossibilities(string.lower())
    possibilities.append(string.lower())
    profane = False
    for i,p in enumerate(possibilities):
        p = letterCompressor(p)
        p = " ".join(wordninja.split(p))
        profane = PROFANITY.contains_profanity(p)
        if profane: break

    if debug and profane:
        print(f"{string} is PROFANE ({possibilities[i]})")
    if debug and not profane:
        print(f"{string} is CLEAN")

    # print(profanity.censor(string))
    # print(profanity.censor(convert))
    return profane, possibilities[i]

def leetPossibilities(username):
    username = unidecode.unidecode(username)
    username = username.replace("l","!")
    allPoss = [username]
    persist = True
    while persist:
        buffPoss = deepcopy(allPoss)
        for poss in buffPoss:
            newPoss, persist = leetBranch(poss)
            allPoss.remove(poss)
            allPoss += newPoss
    return allPoss

def leetBranch(poss):
    branch = []
    leetDict = {
        "13": ["b"],
        "0":["o"],
        "1":["i","l"],
        "2":["s"],
        "3":["e"],
        "4":["a","h"],
        "5":["s"],
        "6":["g","b"],
        "7":["t","j"],
        "8":["b","x"],
        "9":["g","a"],
        "!":["l","i"]
    }
    trigger = False
    for key,value in leetDict.items():
        if key in poss:
            for letter in value:
                branch.append(poss.replace(key,letter,1))
                trigger = True
    if trigger:
        return branch, True
    else:
        return [poss], False


def letterCompressor(string):
    lastChar = [None,None]
    newString = ""
    for c in string:
        if c in ["e","l","o","s","g"]:
            if lastChar[-2] != c:
                newString+=c
        else:
            if lastChar[-1] != c:
                newString+=c
        lastChar.append(c)
    return newString


# import re
# def badPasswordDetect(password):
#     if (len(password)<8): 
#         return True
#     elif not re.search("[a-z]", password): 
#         return True
#     elif not re.search("[A-Z]", password): 
#         return True
#     elif not re.search("[0-9]", password): 
#         return True
#     elif not re.search("[_@$]", password): 
#         return True
#     elif re.search("\s", password): 
#         return True
#     else: 
#         return False




###############################
# OFFLINE / UNIT TESTING
###############################

def profanityGenerator():
    with open("profanity.txt", "r") as f:
        text = f.read()
        rawList = text.split("\n")
        newList = []
        for i, word in enumerate(rawList):
            if not word.isalnum(): continue
            word.replace(" ", "")
            word = letterCompressor(word)
            word = unidecode.unidecode(word)
            wordSplit = " ".join(wordninja.split(word))
            newList.append(word.lower())
            newList.append(wordSplit.lower())

        newList = [*dict.fromkeys(newList)]
        newList.sort()

        with open("new.txt", "w") as ff:
            ff.write("\n".join(newList))


def unitTest():
    with open("census2000.csv", "r") as f:
        with open("bad_name3.txt", "a") as ff:
            content = f.read().split("\n")
            for line in content:
                name = line.split(",")[0].strip()
                if badWordDetector(name):
                    ff.write(name+"\n")


if __name__ == "__main__":
    initPROFANITY(APP=None)
    # unitTest()

    while True:
        word = input("> ")
        report = usernameSniff(word)
        print(report["report"])


    # profanityGenerator()

    #out = emailSniff("Rosemaster3000@gmail.com")
    #print(out)
    #ipInfo("184.88.212.177") # home
    # ipInfo("166.172.184.11") # iphone
    # ipInfo("1.112.0.0")      # japan

    # ipInfo("192.168.86.1")  # LAN
    # ipInfo("127.0.0.1")      # Local Testing





