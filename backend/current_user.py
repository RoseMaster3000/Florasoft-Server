# Flask Dependancies
from flask import render_template, url_for, redirect, request, session, current_app
from flask import Blueprint
from flask_socketio import join_room, disconnect, emit
# Libraries
from datetime import datetime
from passlib.hash import pbkdf2_sha256
# Multi-File
from utility import EPOCH_MS, chronological, cPrint
from backend.database import *
from backend.validate import *



class livelist(list):
    def __init__(self):
        self.refresh()

    def __setitem__(self, index, value):
        list.__setitem__(self, index, value)

    def __getitem__(self, index):
        return self[index]

    def refresh(self):
        list.clear(self)
        for item in distillCollection("user", "username", "sid", "sdid"):
            list.append(self, item)

ALLONLINE = livelist()

user = Blueprint(
    "user",
    __name__,
    template_folder='../templates',
    static_folder='../static')

###############################
# ATTRIBUTES (SET/GET)
###############################

def get(*attributes):
    # get "userDocument"
    if "username" in session:
        userDocument = getDocument("user", "username", session["username"])
    else:
        userDocument = session
    # return single value 
    if len(attributes)==1:
        return userDocument.get(attributes[0], None)
    # return value dict
    else:
        getDict = {}
        for attr in attributes:
            getDict[attr] = userDocument.get(attr, None)
        return getDict

def set(attribute, value):
    if "username" in session:
        setAttribute("user", "username", session["username"], attribute, value)
    else:
        session[attribute] = value

def setMany(updateDict):
    if "username" in session:
        updateDocument("user", "username", session["username"], updateDict)
    else:
        for key, value in updateDict.items():
            session[key] = value 

def rm(attribute):
    if "username" in session:
        rmAttribute("user", "username", session["username"], attribute)
    else:
        del session[attribute]



###############################
# LEAVE/JOIN  (Connect to Socket with Authentication)
###############################

def leave():
    myUsername = get("username")
    if (myUsername != None):
        cPrint(myUsername + " has gone offline.", color="red")
        rm("sid")
        rm("sdid")

        ALLONLINE.refresh()
        print(ALLONLINE)


def join():
    myUsername = get("username")
    if (myUsername != None):
        cPrint(myUsername + " has gone online!", color="green")
        # [dopplegagner kicker]
        # diconnect/logout any users who are currently using this account
        for user in ALLONLINE:
            if user["username"] == myUsername:
                sid = user["sid"]
                sdid = user["sdid"]
                cPrint(f"{myUsername}-doppleganger on socket: {sid}")
                disconnect(sid)
                cPrint(f"revoking that users session: {sdid}")
                dropDocument(f"session", "id", f"session:{sdid}")
        # Associate new session data with user
        set("sid", request.sid)
        set("sdid", request.cookies.get('session'))
        
        ALLONLINE.refresh()
        print(ALLONLINE)

###############################
# LOGIN/OUT + SIGNUP (Get Authentication)
###############################

def logout():
    leave()
    session.clear()
    return True, "Logged Out Successfully"

# form: dict{username, password}
def login(form):
    # lookup user in database
    if "@" in form["username"]:
        userDocument = getDocument("user", "email", form["username"])
    else:
        userDocument = getDocument("user", "username", form["username"])

    # validate username/password
    if userDocument==None:
        return False, "{} does not exist".format(form["username"])
    elif not pbkdf2_sha256.verify(form["password"], userDocument["password"]):
        return False, "Invalid Password"

    # update data from before login (session)
    updates = {}
    for key,value in session.items():
        if not key.startswith("_"): updates[key] = value
    session.clear()
    # update info aabout this login
    ipInfo = ipSniff(request)
    updates["login"] = "Date()"
    updates["latitude"] =  ipInfo["latitude"]
    updates["longitude"] = ipInfo["longitude"]
    updates["ip"] = ipInfo["address"]
    # login
    updateDocument("user", "username", userDocument["username"], updates)
    session["username"] = userDocument["username"]
    return True, userDocument


# form: dict{username, email, password, password_verify}
def register(form): return signup()
def signup(form):
    print("signing up...")
    if session.get("username",None) not in current_app.config["ADMINS"]:
        # validate username
        uSniff = usernameSniff(form["username"])
        if uSniff["valid"] == False:
            return False, "Username Error: "+uSniff["report"]
        # validate password
        if form["password"] != form["password_verify"]:
            return False, "Password Error: Passwords Do Not Match"
        if len(form["password"]) <= 5:
            return False, "Password Error: Password Too Short"
        # validate email
        emInfo = emailSniff(form["email"])
        if emInfo["valid"] == False:
            return False, "Email Error: Email Not Valid"
    
    # INSERT ACCOUNT INFO INTO DB
    data = {}
    data["email"] = emInfo["email"]
    data["username"] = form["username"]
    data["password"] =  pbkdf2_sha256.hash(form["password"])
    data["elo"] = 2000
    ipInfo = ipSniff(request)
    data["latitude"] =  ipInfo["latitude"]
    data["longitude"] = ipInfo["longitude"]
    data["ip"] = ipInfo["address"]
    data["creation"] = "Date()"
    print("adding user to DB...")
    success, report = addDocument("user", data)

    # validate insertion
    if success:
        return True, report
    else:
        return False, f"{report.title()} in use by existing account"


###############################
# MATCHMAKING
###############################

# def marry(self, fiance):
#     # validate
#     if self.partner!=None:
#         raise Excpetion(f"{self} is married to {self.partner} (can't marry {fiance})")
#     if fiance.partner!=None:
#         raise Excpetion(f"{fiance} is married to {fiance.partner} (can't marry {self})")
#     # marry
#     self.partner = fiance
#     self.partner.partner = self

# def divorce(self):
#     # validate
#     if self.partner!=None:
#         raise Excpetion(f"{self} is not married (can't divorce)")
#     # divorce
#     self.partner.partner = None
#     self.partner = None
