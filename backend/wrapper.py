# Flask Dependancies
from flask import render_template, url_for, redirect, request, session
from flask_socketio import join_room, emit, send, disconnect
from flask import Blueprint
from functools import wraps

import backend.current_user as current_user

wrapper = Blueprint(
    "wrapper",
    __name__,
    template_folder='../templates',
    static_folder='../static')


#########################################
# Authentication Wrappers (HTTP)
#########################################

def admin_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if current_user.get("admin"):
            return f(*args, **kwargs)
        else:
            return render_template("error.html", code=403, desc="forbidden: administrators only")
    return wrapper

def alpha_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if current_user.get("alpha"):
            return f(*args, **kwargs)
        elif current_user.get("username"):
            return render_template("error.html", code=401, desc="{} is not in closed alpha".format(current_user.get("username")))
        else:
            return render_template("error.html", code=401, desc="authentication + alpha access required")
    return wrapper

def login_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if current_user.get("username"):
            return f(*args, **kwargs)
        else:
            return render_template("error.html", code=401, desc="authentication required (log in)")
    return wrapper

def anon_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not current_user.get("username") or current_user.get("admin"):
            return f(*args, **kwargs)
        else:
            return render_template("error.html", code=401, desc="anonimity required (log out)")
    return wrapper

#########################################
# Authentication Wrappers (SOCKET)
#########################################

def admin_required_sock(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if current_user.get("sid") and current_user.get("admin"):
            return f(*args, **kwargs)
        else:
            return emit("401", {"message":"administrators only"})
    return wrapper

def alpha_required_sock(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if current_user.get("sid") and current_user.get("alpha"):
            return f(*args, **kwargs)
        elif current_user.get("sid"):
            return emit("401", {"message": "{} is not in closed alpha".format(current_user.get("username"))} )
        else:
            return emit("401", {"message": "authentication + alpha access required"} )
    return wrapper

def login_required_sock(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if current_user.get("sid"):
            return f(*args, **kwargs)
        else:
            return emit("401", {"message":"authentication required (log in)"})
    return wrapper

def anon_required_sock(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not current_user.get("sid"):
            return f(*args, **kwargs)
        else:
            return emit("401", {"message":"anonimity required (log out)"})
    return wrapper