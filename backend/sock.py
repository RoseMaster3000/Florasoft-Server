# Flask Dependancies
from flask import request, session
from flask_socketio import join_room, emit, send, disconnect
from io_blueprint import IOBlueprint

# Multi-File
from utility import cPrint
from backend.wrapper import login_required_sock as login_required
from backend.wrapper import anon_required_sock as anon_required
from backend.wrapper import admin_required_sock as admin_required
from backend.wrapper import alpha_required_sock as alpha_required

import backend.current_user as current_user
from backend.current_user import ALLONLINE

import json

baseSock = IOBlueprint("/")


###############################
# Connector (+auto authenticator)
###############################

@baseSock.on("connect")
def myConnect():
    cPrint("{}~{} has connected...".format(request.remote_addr, request.sid[:5]), color="green")
    # print("sid", request.sid)
    # print("sdid", request.cookies.get('session'))

    # auto login sock (if session already authenticated)
    current_user.join()
    emit('ALLONLINE', ALLONLINE, broadcast=True)


@baseSock.on("disconnect")
def myDisconnect():
    cPrint("{}~{} has disconnected...".format(request.remote_addr, request.sid[:5]), color="red")

    # user has stopped playing (if session is authenticated)
    current_user.leave()
    emit('ALLONLINE', ALLONLINE, broadcast=True)



@baseSock.on("forget") # ask server to forget about user comepletly
def forgetSock():
    sid = current_user.get("sid")
    sdid = current_user.get("sdid")
    if (sid!=None): disconnect(sid)
    if (sdid!=None): dropDocument(f"session", "id", f"session:{sdid}")


###############################
# login / logout
###############################

@baseSock.on("login")
@anon_required
def loginSock(data):
    data = json.loads(data)
    success, report = current_user.login(data)
    if success:
        resp = {"success":True, "message":current_user.get("username")}
        current_user.join()
    else:
        resp = {"success":False,"message":report}
    emit("loginResp", resp)


@baseSock.on("register")
@anon_required
def loginSock(data):
    data = json.loads(data)
    success, report = current_user.register(data)
    if success:
        resp = {"success":True, "message":report}
    else:
        resp = {"success":False,"message":report}
    emit("registerResp", resp)

    # login/connect after register
    # if success:
        # success, report = current_user.login(data)
        # current_user.join()
        # resp = {"success":True, "message":current_user.get("username")}
        # emit("loginResp", resp)


@baseSock.on("logout") # diconnect and logout
@login_required
def logoutSock():
    current_user.leave()
    current_user.logout()

###############################
# basic questions
###############################

@baseSock.on("iam")
# @login_required
def iamSock():
    name = current_user.get("username")
    if (name == None):
        emit("iam", {"username":""})
    else:
        emit("iam", {"username":name})


@baseSock.on("message")
def messageSock(data):
    cPrint(f"in-message: {data}", color="blue")
    emit("message", {"bingo":"just a test","num":23})



@baseSock.on("version")
def versionSock():
    emit("version", {"version":0.2})


