**[- This project is no longer the deployed and source code is EOL. -]**

# Overview
This is the OLD source code for [florasoft.live](https://florasoft.live) & [shahrose.com](http://shahrose.com). The future plans for my personal website is as follows:
* The domain `florasoft.live` will not be renewed 
* A new web server built with Mongo, Node, and [SvelteKit](https://kit.svelte.dev/docs/introduction) or [Nuxt3](https://nuxt.com/docs/getting-started/introduction)
* The new web server will be deployed on `shahrose.com` and/or `rosemaster3000.com`

### Development Stack
It uses a non traditional stack I call the "LMNOP Stack":
* Linux
* MongoDB
* Nginx
* Old Vue (2.x)
* Python (Flask)

All jokes aside, I do like this stack. I find Vue a very simple/beautiful front end (thanks to Vuetify) and Python a very simple/powerful backend (thanks to modules/pip). The backend also supports [Socket.io](https://flask-socketio.readthedocs.io/en/latest/), which is used for real time apps & games I develop.


[Sunbiz Renew](https://dos.myflorida.com/sunbiz/manage-business/efile/annual-report/)
[Sunbix Entity](http://search.sunbiz.org/Inquiry/CorporationSearch/SearchResultDetail?inquirytype=EntityName&directionType=Initial&searchNameOrder=FLORASOFT%20L190000162180&aggregateId=flal-l19000016218-4aeed24f-4180-4c94-ac39-ff87eefc6c65&searchTerm=Florasoft&listNameOrder=FLORASOFT%20L190000162180)


# Setup
### Project Files
1. Install Python 3.9 `paru python39` ([TimeoutError](https://github.com/eventlet/eventlet/issues/687) is Immutable in Python 3.10+)
2. Clone repository `git clone https://gitlab.com/RoseMaster3000/Florasoft-Server.git`
3. Populate `SECRETS.py` file (use `SECRETS.py.template` as a guide)
### Large Files (LFS)
4. Install git LFS `paru git-lfs` or `apt-get install git-lfs`
5. Activate git LFS `git lfs install`
6. Clone Large Files `git lfs pull`
### Virtual Environment
7. Create virtual environment `python3.9 -m venv virt`
8. Enter virtual environment `source virt.sh`
9. Install requirments `(virt) pip install -r requirements.txt`
10. Install blueprint module `(virt) pip install git+http://github.com/m-housh/io-blueprint.git`
### Launch Points
* Local Testing (Port:5000) `(virt) python testlocal.py`
* LAN Testing (Port:4200) `(virt) python testlan.py`
* WSGI Testing (Port:8080) `(virt) python wsgi.py`


# Deployment
### GUNICORN
11. Install Gunicorn `(virt) pip install gunicorn==20.1.0`
12. Exit virtual environment `deactivate`
13. Test Gunicorn `/home/shahrose/Florasoft-Server/virt/bin/gunicorn wsgi:app --worker-class gevent --workers 1 --bind 0.0.0.0:8080`
14. Create Gunicorn service file `nano /etc/systemd/system/florasoft-aa.service`  (refer to production/florasoft-aa.service)
15. Verify Gunicorn service `source gunicorn.sh`
16. Activate Gunicorn service `systemctl start florasoft-aa` and `systemctl enable florasoft-aa`
### NGINX
17. Install Nginx `paru nginx`
18. Edit NGINX base config `/etc/nginx/nginx.conf` (refer to production/nginx.conf)
19. Create NGINX site config `/etc/nginx/sites-available/florasoft.conf` (refer to production/florasoft.conf)
20. Enable NGINX site config `ln -s /etc/nginx/sites-available/florasoft.conf  /etc/nginx/sites-enabled/florasoft.conf`
21. Verify NGINX config `nginx -t`
22. Verify NGINX service `source nginx.sh`
23. Activate NGINX Service `systemctl start nginx` and `systemctl enable nginx`
### HTTPS (CertBot)
24. Install CertBot `paru certbot` and `paru certbot-nginx`
25. Activate CertBot `certbot --nginx -d florasoft.live -d shahrose.com`
26. Test CertBot's auto-renewal CRON job `certbot renew --dry-run`
### Firewall (UFW)
27. Install Uncomplicated Firewall `paru ufw`
28. Allow all out-traffic `ufw default allow outgoing`
29. Whitelist NGINX in-traffic `ufw allow 'Nginx HTTPS'`  (HTTP/HTTPS/Full)
30. Whitelist MongoDB in-traffic `ufw allow 27015:27017`
31. Whitelist E-Mail in-traffic `ufw allow 25,587,465,2525/tcp`
32. Whitelist LAN developer in-traffic `ufw allow from 192.168.100.109`
33. Blacklist all other in-traffic `ufw default deny incoming`
34. Verify UFW configuration `ufw status`
35. Activate UFW `ufw enable`

# Maintenance
### Service Control
* Update Production `source update.sh` (git pull origin master)
* Restart Production `source restart.sh`
* Stop Production `source stop.sh`

### MongoDB Notes
* Our DB is hosted on MongoAtlas (MongoDB 4.4.12 - GCP:europe-west1)
* External IP's will need to be explicitly white-listed on MongoAtlas.
* DB requires uses a "user" collection with collated/unique "email" and "username" fields.
* DB requires uses a "log" collection which is capped with a size of 100000.


# Documentation Portal
### Vuetify
* [Wireframes](https://vuetifyjs.com/en/getting-started/wireframes/)
* [Components](https://vuetifyjs.com/en/components/application/)
* [Animations](https://vuetifyjs.com/en/features/scrolling/)
* [Themes](https://theme-generator.vuetifyjs.com/)
* [Icons](https://materialdesignicons.com/)
* [Figma](https://www.figma.com/community/file/967114083319278799)

### Vue Grid
* [Container](https://vuetifyjs.com/en/api/v-container/#props)
* [Row](https://vuetifyjs.com/en/api/v-row/#props)
* [Col](https://vuetifyjs.com/en/api/v-col/)
* [Spacer](https://vuetifyjs.com/en/api/v-spacer/)
* [Card](https://vuetifyjs.com/en/api/v-card/)

### Micro Services
* [IP Info](https://ipinfo.io/missingauth)
* [IP City Database](https://github.com/maxmind/GeoIP2-python#city-database)
* [Nations](https://raw.githubusercontent.com/risan/country-flag-emoji-json)
* [Currencies](http://country.io)
* [Open Exchange Rates](http://openexchangerates.org)
* [Exchange Rates](http://api.exchangeratesapi.io)

### Frameworks
* [Flask](https://flask.palletsprojects.com/en/2.0.x/)
* [Vue.js](https://v2.vuejs.org/v2/guide/)
* [Mongo.db](https://account.mongodb.com/account/login)
* [Animation Easings](https://easings.net/)


# Author
Shahrose Kasim  
[rosemaster3000@gmail.com](mailto:rosemaster3000@gmail.com)  
[RoseMaster#3000](https://discordapp.com/)  